use std::net::TcpStream;
use std::sync::mpsc::{Receiver, channel};
use std::cell::RefCell;

use gtk::prelude::*;

use libappindicator::{
	AppIndicator,
	AppIndicatorStatus
};

use clap::Parser;

mod protocol;

use protocol::packets::*;
use protocol::Packet;


thread_local! {
	static APP: RefCell<Option<MCServerStatusApp>> = RefCell::new(None);
}

struct MCServerStatusApp {
	menu:          gtk::Menu,
	app_indicator: AppIndicator,
	msg_rx:        Receiver<Message>
}

enum Message {
	StatusUpdate(Option<StatusResponse>)
}

#[derive(Parser, Debug)]
#[clap(author, version, about)]
pub struct Args {
	#[clap(short, long, value_parser)]
	hostname: String,

	#[clap(short, long, value_parser, default_value_t = 25565)]
	port: u16,

	#[clap(short, long, value_parser, default_value_t = 300)]
	update_interval: u64
}

fn main() -> Result<(), Box<dyn std::error::Error>> {

	let args = Args::parse();

	gtk::init()?;

	let (tx, rx) = channel();

	let mut status_app = MCServerStatusApp {
		menu:          gtk::Menu::new(),
		app_indicator: AppIndicator::new("MC Server Status", ""),
		msg_rx:        rx
	};

	status_app.app_indicator.set_status(AppIndicatorStatus::Active);
	status_app.app_indicator.set_menu(&mut status_app.menu);
	status_app.menu.show_all();

	APP.with(|state| {
		*state.borrow_mut() = Some(status_app);
	});

	std::thread::spawn(move || {

		loop {

			let status = get_minecraft_server_status(&args.hostname, args.port);
			let msg = Message::StatusUpdate(status.ok());

			tx.send(msg).unwrap();

			glib::timeout_add_seconds_once(0, || {
				
				update_ui();
			});

			std::thread::sleep(std::time::Duration::from_secs(args.update_interval));
		}
	});

	gtk::main();

	Ok(())
}

fn update_ui() {

	APP.with(|app_ref| {

		if let Some(app) = &mut *app_ref.borrow_mut() {

			for child in app.menu.children() {
				app.menu.remove(&child);
			}

			match app.msg_rx.recv().unwrap() {
				Message::StatusUpdate(status) => {

					if let Some(status_response) = status {

						app.app_indicator.set_icon("icon_online.png");

						app.menu.append(&gtk::MenuItem::with_label("Server Online"));
		
						app.menu.append(&gtk::MenuItem::with_label("Server Online"));
		
						app.menu.append(&gtk::MenuItem::with_label(&status_response.description.to_string()));
						app.menu.append(&gtk::SeparatorMenuItem::new());
		
						app.menu.append(&gtk::MenuItem::with_label(&format!("Version: {}", status_response.version.name)));
						app.menu.append(&gtk::SeparatorMenuItem::new());
		
						app.menu.append(&gtk::MenuItem::with_label(&format!("Online: {}/{}", status_response.players.online, status_response.players.max)));
						app.menu.append(&gtk::SeparatorMenuItem::new());
		
						if let Some(players) = status_response.players.sample {
							for player in players.iter()
							.take(20) {
		
								app.menu.append(&gtk::MenuItem::with_label(&player.name));
							}
						}
		
					} else {

						app.app_indicator.set_icon("icon_offline.png");

						app.menu.append(&gtk::MenuItem::with_label("Server Offline"));
		
					}
				},
				
			}

			app.menu.show_all();
		}
	})
}

fn get_minecraft_server_status(hostname: &str, port: u16) -> std::io::Result<StatusResponse> {

	let mut tcpsocket = TcpStream::connect(&format!("{}:{}", hostname, port))?;

	Handshake::new(-1, hostname, port, 1).mcpacket_write(&mut tcpsocket)?;

	StatusRequest.mcpacket_write(&mut tcpsocket)?;

	StatusResponse::mcpacket_read(&mut tcpsocket)
}
