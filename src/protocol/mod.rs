pub mod dtypes;
pub mod packets;

use std::io::{Read, Write, Error as IOError};

use dtypes::VarInt;


pub trait MinecraftProtocol: Sized {

	fn mcp_read<R: Read>(src: &mut R) -> Result<Self, IOError>;
	fn mcp_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError>;
	fn num_bytes(&self) -> usize;
}


pub trait Packet: Sized {

	fn id(&self) -> i32;
	fn len(&self) -> i32;

	fn mcpacket_decode<R: Read>(src: &mut R) -> Result<Self, IOError>;
	fn mcpacket_encode<W: Write>(&self, dst: &mut W) -> Result<(), IOError>;

	fn mcpacket_read<R: Read>(src: &mut R) -> Result<Self, IOError> {

		VarInt::mcp_read(src)?;
		VarInt::mcp_read(src)?;

		Self::mcpacket_decode(src)
	}

	fn mcpacket_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError> {

		let mut buf: Vec<u8> = vec![];

		let id = VarInt::new(self.id());

		VarInt::new(self.len() + id.num_bytes() as i32).mcp_write(&mut buf)?;

		id.mcp_write(&mut buf)?;

		self.mcpacket_encode(&mut buf)?;

		dst.write(&buf)?;

		Ok(())
	}
}
