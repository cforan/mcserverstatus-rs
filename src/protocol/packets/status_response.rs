use std::io::{Read, Write, Error as IOError};

use serde::{Serialize, Deserialize};

use crate::protocol::Packet;
use crate::protocol::MinecraftProtocol;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct StatusResponse {
	pub version: Version,
	pub players: Players,
	pub description: Chat, // uses the same json structure as in-game chat
	#[serde(skip_serializing_if = "Option::is_none")]
	pub favicon: Option<String>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub previews_chat: Option<bool>
}

impl Packet for StatusResponse {

	fn id(&self) -> i32 { 0x00 }
	fn len(&self) -> i32 {
		
		let mut buf: Vec<u8> = vec![];
		self.mcpacket_encode(&mut buf).unwrap();

		buf.len() as i32
	}

	fn mcpacket_decode<R: Read>(src: &mut R) -> Result<Self, IOError> {
		
		serde_json::from_str(&String::mcp_read(src)?)
			.map_err(|e| IOError::from(e))
	}

	fn mcpacket_encode<W: Write>(&self, dst: &mut W) -> Result<(), IOError> {
		
		serde_json::to_string(self)
			.map_err(|e| IOError::from(e))?	
			.mcp_write(dst)
	}
}


#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Version {
	pub name:     String,
	pub protocol: i32,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Players {
	pub max:    i32,
	pub online: i32,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub sample: Option<Vec<Player>>
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Player {
	pub name: String,
	pub id:   String
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
#[serde(untagged)]
pub enum Chat {
	Component(ChatComponent),
	String(String)
}

impl std::fmt::Display for Chat {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		
		match self {
			Chat::String(s) => write!(f, "{}", s),
			Chat::Component(c) => {
				write!(f, "{}", c.text)?;
				if let Some(e) = &c.extra {
					for ec in e {
						write!(f, "{}", ec)?
					}
				}
				Ok(())
			}
		}
	}
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct ChatComponent {
	text:          String,
	#[serde(skip_serializing_if = "Option::is_none")]
	bold:          Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	italic:        Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	underlined:    Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	strikethrough: Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	obfuscated:    Option<bool>,
	#[serde(skip_serializing_if = "Option::is_none")]
	font:          Option<String>,
	#[serde(skip_serializing_if = "Option::is_none")]
	color:         Option<String>,
	#[serde(skip_serializing_if = "Option::is_none")]
	insertion:     Option<String>,
	#[serde(skip_serializing_if = "Option::is_none")]
	click_event:   Option<serde_json::Value>, // these two events can be ignored, they won't be used for server status
	#[serde(skip_serializing_if = "Option::is_none")]
	hover_event:   Option<serde_json::Value>,
	#[serde(skip_serializing_if = "Option::is_none")]
	extra:         Option<Vec<Chat>>
}

impl std::fmt::Display for ChatComponent {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		
		write!(f, "{}", self.text)?;

		if let Some(e) = &self.extra {
			for c in e {
				write!(f, "{}", c)?
			}
		}

		Ok(())
	}
}

impl Default for ChatComponent {
	fn default() -> Self {
		Self {
			text:          String::new(),
			bold:          None,
			italic:        None,
			underlined:    None,
			strikethrough: None,
			obfuscated:    None,
			font:          None,
			color:         None,
			insertion:     None,
			click_event:   None,
			hover_event:   None,
			extra:         None
		}
	}
}

#[cfg(test)]
mod tests {

	use super::*;

	#[test]
	fn test_server_status_serialize() {

		// surely there's a better way to format this?
		let expected = String::from("{\"version\":{\"name\":\"1.19\",\
\"protocol\":759},\"players\":{\"max\":25,\"online\":1,\"sample\":[{\"name\":\
\"username\",\"id\":\"01234567-890a-bcde-f01234567890\"}]},\"description\":\
{\"text\":\"Hello World!\"},\"previewsChat\":true}");

		let value = StatusResponse {
			version: Version {
				name: String::from("1.19"),
				protocol: 759
			},
			players: Players {
				max: 25,
				online: 1,
				sample: Some(vec![
					Player {
						name: String::from("username"),
						id: String::from("01234567-890a-bcde-f01234567890")
					}
				])
			},
			description: Chat::Component(ChatComponent {
				text: String::from("Hello World!"),
				..Default::default()
			}),
			previews_chat: Some(true),
			favicon: None
		};

		let serialized = serde_json::to_string(&value).unwrap();

		assert_eq!(expected, serialized);
	}

	#[test]
	fn test_server_status_deserialize() {

		let expected = StatusResponse {
			version: Version {
				name: String::from("1.19"),
				protocol: 759
			},
			players: Players {
				max: 25,
				online: 1,
				sample: Some(vec![
					Player {
						name: String::from("username"),
						id: String::from("01234567-890a-bcde-f01234567890")
					}
				])
			},
			description: Chat::Component(ChatComponent {
				text: String::from("Hello World!"),
				..Default::default()
			}),
			previews_chat: Some(true),
			favicon: None
		};

		let serialized = String::from("{\"version\":{\"name\":\"1.19\",\
\"protocol\":759},\"players\":{\"max\":25,\"online\":1,\"sample\":[{\"name\":\
\"username\",\"id\":\"01234567-890a-bcde-f01234567890\"}]},\"description\":\
{\"text\":\"Hello World!\"},\"previewsChat\":true}");

		let deserialized: StatusResponse = serde_json::from_str(&serialized).unwrap();

		assert_eq!(deserialized, expected);
	}
}
