mod handshake;
mod status_request;
mod status_response;

pub use handshake::Handshake;
pub use status_request::StatusRequest;
pub use status_response::StatusResponse;

/*

// used while testing with a local server

#[cfg(test)]
mod tests {

	use super::*;
	use super::super::Packet;

	use std::net::{TcpStream};

	#[test]
	fn test_server_status() {

		let hostname = "172.17.0.3";
		let port: u16 = 25565;

		let mut tcpsocket = TcpStream::connect(&format!("{}:{}", hostname, port)).unwrap();
		tcpsocket.set_read_timeout(Some(std::time::Duration::new(35, 0))).unwrap();

		let handshake_pkt = Handshake::new(-1, hostname, port, 1);

		handshake_pkt.mcpacket_write(&mut tcpsocket).unwrap();

		StatusRequest.mcpacket_write(&mut tcpsocket).unwrap();

		let status_response = StatusResponse::mcpacket_read(&mut tcpsocket).unwrap();

		println!("{:#?}", status_response);

	}
}
*/
