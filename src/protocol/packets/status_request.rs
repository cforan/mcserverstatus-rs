use std::io::{Read, Write, Error as IOError};

use crate::protocol::Packet;

pub struct StatusRequest;

impl Packet for StatusRequest {

	fn id(&self) -> i32 { 0x00 }
	fn len(&self) -> i32 { 0 }

	fn mcpacket_decode<R: Read>(_src: &mut R) -> Result<Self, IOError> {

		Ok(Self)
	}

	fn mcpacket_encode<W: Write>(&self, _dst: &mut W) -> Result<(), IOError> {
		
		Ok(())
	}
}
