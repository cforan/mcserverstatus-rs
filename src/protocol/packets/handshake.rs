use std::io::{Read, Write, Error as IOError};

use crate::protocol::Packet;
use crate::protocol::VarInt;
use crate::protocol::MinecraftProtocol;

#[derive(Debug)]
pub struct Handshake {
	pub proto_version: i32,
	pub server_addr:   String,
	pub server_port:   u16,
	pub state:         i32
}

impl Handshake {

	pub fn new (proto_version: i32, server_addr: &str, server_port: u16, state: i32) -> Self {

		Self {
			proto_version,
			server_addr: String::from(server_addr),
			server_port,
			state
		}
	}
}

impl Packet for Handshake {

	fn id(&self) -> i32 { 0x00 }
	fn len(&self) -> i32 {

		(VarInt::new(self.proto_version).num_bytes() +
		self.server_addr.num_bytes() +
		self.server_port.num_bytes() +
		VarInt::new(1).num_bytes()) as i32
	}

	fn mcpacket_decode<R: Read>(src: &mut R) -> Result<Self, IOError> {

		Ok(Handshake {
			proto_version: VarInt::mcp_read(src)?.into(),
			server_addr:   String::mcp_read(src)?,
			server_port:   u16::mcp_read(src)?,
			state:         VarInt::mcp_read(src)?.into()
		})
	}

	fn mcpacket_encode<W: Write>(&self, dst: &mut W) -> Result<(), IOError> {
		
		VarInt::new(self.proto_version).mcp_write(dst)?;
		self.server_addr.mcp_write(dst)?;
		self.server_port.mcp_write(dst)?;
		
		VarInt::new(1).mcp_write(dst)
	}
}
