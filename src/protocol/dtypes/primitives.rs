use std::io::{Read, Write, Error as IOError};

use byteorder::{BigEndian, WriteBytesExt, ReadBytesExt};

use super::super::MinecraftProtocol;

impl MinecraftProtocol for i8 {
	
	fn mcp_read<R: Read>(src: &mut R) -> Result<Self, IOError> { src.read_i8() }
	fn mcp_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError> { dst.write_i8(*self) }
	fn num_bytes(&self) -> usize { 1 }
}

impl MinecraftProtocol for u8 {

	fn mcp_read<R: Read>(src: &mut R) -> Result<Self, IOError> { src.read_u8() }
	fn mcp_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError> { dst.write_u8(*self) }
	fn num_bytes(&self) -> usize { 1 }
}

impl MinecraftProtocol for i16 {

	fn mcp_read<R: Read>(src: &mut R) -> Result<Self, IOError> { src.read_i16::<BigEndian>() }
	fn mcp_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError> { dst.write_i16::<BigEndian>(*self) }
	fn num_bytes(&self) -> usize { 2 }
}

impl MinecraftProtocol for u16 {

	fn mcp_read<R: Read>(src: &mut R) -> Result<Self, IOError> { src.read_u16::<BigEndian>() }
	fn mcp_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError> { dst.write_u16::<BigEndian>(*self) }
	fn num_bytes(&self) -> usize { 2 }
}

impl MinecraftProtocol for i32 {

	fn mcp_read<R: Read>(src: &mut R) -> Result<Self, IOError> { src.read_i32::<BigEndian>() }
	fn mcp_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError> { dst.write_i32::<BigEndian>(*self) }
	fn num_bytes(&self) -> usize { 4 }
}

impl MinecraftProtocol for i64 {

	fn mcp_read<R: Read>(src: &mut R) -> Result<Self, IOError> { src.read_i64::<BigEndian>() }
	fn mcp_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError> { dst.write_i64::<BigEndian>(*self) }
	fn num_bytes(&self) -> usize { 8 }
}

impl MinecraftProtocol for f32 {

	fn mcp_read<R: Read>(src: &mut R) -> Result<Self, IOError> { src.read_f32::<BigEndian>() }
	fn mcp_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError> { dst.write_f32::<BigEndian>(*self) }
	fn num_bytes(&self) -> usize { 4 }

}

impl MinecraftProtocol for f64 {

	fn mcp_read<R: Read>(src: &mut R) -> Result<Self, IOError> { src.read_f64::<BigEndian>() }
	fn mcp_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError> { dst.write_f64::<BigEndian>(*self) }
	fn num_bytes(&self) -> usize { 8 }
}

impl MinecraftProtocol for u128 {

	fn mcp_read<R: Read>(src: &mut R) -> Result<Self, IOError> {

		Ok((src.read_u64::<BigEndian>()? as u128) << 64 | src.read_u64::<BigEndian>()? as u128)
	}

	fn mcp_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError> {

		dst.write_u64::<BigEndian>((self >> 64) as u64)?;
		dst.write_u64::<BigEndian>((self & 0xFFFFFFFFFFFFFFFF) as u64)
	}
	
	fn num_bytes(&self) -> usize { 16 }
}

#[cfg(test)]
mod tests {

	use super::*;

	#[test]
	fn test_read_u128() {

		let bytes = vec![
			0xC6, 0x44, 0x95, 0x11, 0x02, 0xF1, 0xFC, 0x30,
			0xD3, 0x7B, 0x5D, 0xFF, 0x13, 0x61, 0x5B, 0xD9
		];
		let expected: u128 = 0xC644951102F1FC30D37B5DFF13615BD9;

		let value = u128::mcp_read(&mut bytes.as_slice()).unwrap();

		assert_eq!(value, expected);
	}

	#[test]
	fn test_write_u128() {

		let value: u128 = 0xC644951102F1FC30D37B5DFF13615BD9;
		let expected = vec![
			0xC6, 0x44, 0x95, 0x11, 0x02, 0xF1, 0xFC, 0x30,
			0xD3, 0x7B, 0x5D, 0xFF, 0x13, 0x61, 0x5B, 0xD9
		];

		let mut bytes: Vec<u8> = vec![];

		value.mcp_write(&mut bytes).unwrap();

		assert_eq!(bytes, expected);
	}
}
