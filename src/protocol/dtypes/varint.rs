use std::io::{Read, Write, Error as IOError, ErrorKind as IOErrorKind};

use byteorder::{WriteBytesExt, ReadBytesExt};

use super::super::MinecraftProtocol;

#[derive(Debug, Clone, Copy)]
pub struct VarInt(i32);

impl VarInt {

	pub fn new(value: i32) -> Self { Self(value) }
}

impl MinecraftProtocol for VarInt {

	fn mcp_read<R: Read>(src: &mut R) -> Result<Self, IOError> {
		
		let mut value: u32 = 0;
		let mut position = 0;

		loop {

			let cbyte: u8 = src.read_u8()?;

			value |= ((cbyte & 0x7F) as u32) << position;
			
			if cbyte & 0x80 == 0 { break; }

			position += 7;
			if position >= 32 {
				return Err(IOError::new(IOErrorKind::InvalidData, "Too many bytes in VarInt"))
			}

		}

		Ok(Self(value as i32))
	}

	fn mcp_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError> {
		
		let mut value = {
			let v: i32 = self.into();
			v as u32
		};

		loop {

			if (value & !0x7Fu32) == 0 {
				dst.write_u8(value as u8)?;
				break;
			}

			dst.write_u8(((value & 0x7F) | 0x80) as u8)?;
			value >>= 7;
		}

		Ok(())
	}

	fn num_bytes(&self) -> usize {

		let mut value = {
			let v: i32 = self.into();
			v as u32
		};

		let mut len = 0;

		loop {

			len += 1;
			if (value & !0x7Fu32) == 0 { break; }
			value >>= 7;
		}

		len
	}
}

impl Into<i32> for &VarInt {

	fn into(self) -> i32 { self.0 }
}

impl Into<i32> for VarInt {

	fn into(self) -> i32 { self.0 }
}

#[cfg(test)]
mod tests {

	use super::*;

	macro_rules! test_read {
		($($name:ident: $data:expr),*) => {

			$(
				#[test]
				fn $name() {
					let(bytes, expected) = $data;
					let value: i32 = VarInt::mcp_read(&mut bytes.as_slice()).unwrap().into();

					assert_eq!(value, expected);
				}
			)*
		};
	}

	macro_rules! test_write {
		($($name:ident: $data:expr),*) => {

			$(
				#[test]
				fn $name() {
					let(expected, value) = $data;
					let mut bytes: Vec<u8> = vec![];

					VarInt::new(value).mcp_write(&mut bytes).unwrap();

					assert_eq!(bytes, expected);
				}
			)*
		};
	}

	test_read! {
		read_0:     (vec![0x00],                         0),
		read_5:     (vec![0x05],                         5),
		read_127:   (vec![0x7F],                         127),
		read_128:   (vec![0x80, 0x01],                   128),
		read_255:   (vec![0xFF, 0x01],                   255),
		read_n1:    (vec![0xFF, 0xFF, 0xFF, 0xFF, 0x0F], -1),
		read_25565: (vec![0xDD, 0xC7, 0x01],             25565),
		read_25651: (vec![0xB3, 0xC8, 0x01],             25651)
	}

	test_write! {
		write_0:     (vec![0x00],                         0),
		write_5:     (vec![0x05],                         5),
		write_127:   (vec![0x7F],                         127),
		write_128:   (vec![0x80, 0x01],                   128),
		write_255:   (vec![0xFF, 0x01],                   255),
		write_n1:    (vec![0xFF, 0xFF, 0xFF, 0xFF, 0x0F], -1),
		write_25565: (vec![0xDD, 0xC7, 0x01],             25565),
		write_25651: (vec![0xB3, 0xC8, 0x01],             25651)
	}
}
