use std::io::{Read, Write, Error as IOError};

use super::super::MinecraftProtocol;

use super::varint::VarInt;

impl MinecraftProtocol for String {

	fn mcp_read<R: Read>(src: &mut R) -> Result<Self, IOError> {

		let len: i32 = VarInt::mcp_read(src)?.into();

		let mut buf = vec![0u8; len as usize];
		src.read_exact(&mut buf[..])?;

		Ok(String::from_utf8(buf).unwrap())
	}

	fn mcp_write<W: Write>(&self, dst: &mut W) -> Result<(), IOError> {
		
		let len = VarInt::new(self.len() as i32);

		len.mcp_write(dst)?;
		dst.write_all(self.as_bytes())
	}

	fn num_bytes(&self) -> usize {

		VarInt::new(self.len() as i32).num_bytes() + self.len()
	}
}

#[cfg(test)]
mod tests {

	use super::*;

	macro_rules! test_read {
		($($name:ident: $data:expr),*) => {
			$(
				#[test]
				fn $name() {
					let (bytes, expected) = $data;
					let value = String::mcp_read(&mut bytes.as_slice()).unwrap();
	
					assert_eq!(value, expected);
				}
			)*
		};
	}

	macro_rules! test_write {
		($($name:ident: $data:expr),*) => {
			$(
				#[test]
				fn $name() {
					let (expected, value) = $data;
					let mut bytes: Vec<u8> = vec![];

					String::from(value).mcp_write(&mut bytes).unwrap();

					assert_eq!(bytes, expected);
				}
			)*
		};
	}

	
	test_read! {
		read_short_str: (vec![0x05, 0x68, 0x65, 0x6C, 0x6C, 0x6F], "hello")
	}

	test_write! {
		write_short_str: (vec![0x05, 0x68, 0x65, 0x6C, 0x6C, 0x6F], "hello")
	}
}
