pub mod primitives;
pub mod string;

mod varint;
mod varlong;

pub use varint::VarInt;
pub use varlong::VarLong;
